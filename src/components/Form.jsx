import React, { Component } from "react";

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      a: 0,
      b: 0,
      choice: 1,
      items: [],
    };
  }

  handleInputChange = (event) => {
    this.setState({
      [event.target.name]: Number(event.target.value),
    });
    console.log(this.state.a);
    console.log(this.state.b);
  };

  calculation = (event) => {
    event.preventDefault();
    const a = this.state.a;
    const b = this.state.b;
    let items = this.state.items;
    const choice = this.state.choice;
    let result = 0;

    if (Number.isNaN(a) || Number.isNaN(b)) {
      alert("Not a valid number!");
      result = null;
    } else if (choice === 1) {
      result = a + b;
    } else if (choice === 2) {
      result = a - b;
    } else if (choice === 3) {
      result = a * b;
    } else if (choice === 4) {
      result = a / b;
    } else if (choice === 5) {
      result = a % b;
    }

    if (result !== null) {
      items.push(result);
      this.setState({
        items: items,
      });
    }

    // console.log(choice);
  };

  render() {
    return (
      <div className="row">
        <div className="col-md-6 mt-5">
          <div className="card">
            <img
              src="https://purepng.com/public/uploads/large/purepng.com-calculator-icon-android-lollipopsymbolsiconsgooglegoogle-iconsandroid-lollipoplollipop-iconsandroid-50-721522597141natii.png"
              alt="calculator icon"
              className="img-fluid"
            />
            <div className="card-body">
              <form className="form-group">
                <div>
                  <input
                    type="text"
                    name="a"
                    className="form-control"
                    placeholder="Input value for A"
                    onChange={this.handleInputChange}
                  />
                </div>
                <div>
                  <input
                    type="text"
                    name="b"
                    className="form-control mt-3"
                    placeholder="Input value for B"
                    onChange={this.handleInputChange}
                  />
                </div>
                <div>
                  <select
                    name="choice"
                    className="form-control mt-3"
                    onChange={this.handleInputChange}
                  >
                    <option value="1">+ Addition</option>
                    <option value="2">- Substraction</option>
                    <option value="3">* Multiplication</option>
                    <option value="4">/ Division</option>
                    <option value="5">% Modulo</option>
                  </select>
                </div>
                <button
                  className="btn btn-primary mt-3"
                  onClick={this.calculation}
                >
                  Calculate
                </button>
              </form>
            </div>
          </div>
        </div>
        <div className="col-md-6 mt-5">
          <ul className="list-group">
            <li className="list-group-item list-group-item-warning">Result</li>
            {this.state.items.map((item, i) => {
              return <li className="list-group-item">{item}</li>;
              // <ListItem item={item} />;
            })}
          </ul>
        </div>
      </div>
    );
  }
}
